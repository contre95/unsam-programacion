/*
Ejemplo de uniones y estructuras
Programacion UNSAM
Autor: David Lopez
Año: 2017
*/

#include <stdio.h>

union uni {
	short s;
	char bytes [2];
};

int main() 
{
	union uni u1;

	u1.bytes [0] = 'A';
	u1.bytes [1] = 'B';

	//Muestro el campo de tipo short en hexadecimal
	printf ("s: %X\n", u1.s);
	
	return 0;
}
