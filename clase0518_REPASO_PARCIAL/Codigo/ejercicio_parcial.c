/*
Programacion UNSAM
Resolucion de parcial de ejemplo (tomado en 2013)
En un laboratorio nuclear se tiene el archivo muestras.dat conformado por una cantidad
indeterminada de n�meros enteros. Estos n�meros est�n en el rango entre 10^2 y 10^7 y
representan el tiempo en segundos que tarda en desintegrarse una muestra.
No puede limitar la cantidad de n�meros.
Se pide hacer un programa que realice las siguientes tareas:
I. Leer el archivo conformando una lista simple. Los nodos de la lista deben quedar en el
mismo orden que en el archivo del que fueron le�dos.
II. Presentar un men� con las siguientes opciones:
1- Agregar un nuevo elemento al principio de la lista.
2- Eliminar el primer elemento que coincida con un valor ingresado por teclado.
3- Informar el tiempo de desintegraci�n m�s bajo de la lista.
4- Mostrar todos los tiempos de desintegraci�n que son mayores a un valor ingresado
por teclado e informar cu�ntos fueron.
5- Fin del men�.
Nota: Para cada opci�n del men� (excepto 5) se deber� usar al menos una funci�n.

Autor: David Lopez
A�o: 2017
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct nodo{
    int dato;
    struct nodo *sig;        
} nodo;

//Inserta al principio
nodo *insertar_lifo (nodo *l, int d) {
	nodo *nuevo;

	nuevo = (nodo*) malloc (sizeof (nodo));
	nuevo->dato = d;
	nuevo->sig = l;
	return nuevo;
	}	

//Inserta al final
nodo *insertar_fifo (nodo *l, int d) {
	nodo *nuevo, *p;

	nuevo = (nodo*) malloc (sizeof (nodo));
	nuevo->dato = d;
	nuevo->sig = NULL;  //Porque va a ser el ultimo nodo
	if (l == NULL)
		return nuevo;
	//Como la lista no esta vacia la recorro buscando el ultimo nodo
	p = l;
	while (p->sig != NULL)
		p = p->sig;
	//Ahora p es el ultimo nodo
	p->sig = nuevo;
	return l;
	}

//Elimina un nodo de la lista
nodo *eliminar (nodo* l, int d)	{
	nodo* ret=l, *aux;

	if(l!=NULL)	{
		if(l->dato==d){
			ret=l->sig;
			free(l);
		} 
		else {
			while(l->sig!=NULL && l->sig->dato!=d)
				l=l->sig;
			if(l->sig!=NULL) {
				aux=l->sig;
				l->sig=aux->sig;
				free(aux);
			}
		}
	}
	return ret;
}

//Libera toda la memoria ocupada por la lista
nodo *destruir(nodo *l){
    nodo * aux;
	while (l != NULL) {
		aux = l;
		l = l->sig;
		free (aux);
	}
	printf ("lista destruida\n");
	return NULL;
}

//Opcion 1 del menu
nodo *agregar(nodo *l){
    int n;
    printf("\nIngrese el nro: ");
    scanf("%d", &n);
	//Devuelve la lista actualizada agregando al principio
    return insertar_lifo(l, n);
}

//Opcion 2 del menu
nodo *borrar(nodo *l){
    int n;
    printf("\nIngrese el nro: ");
    scanf("%d", &n);
    return eliminar(l, n);
}

//Opcion 3 del menu
void minimo(nodo *l){
    nodo *p;
    int min;
	
    if(l==NULL)
        printf("\nLista vacia");
    else{
        min = l->dato;
        p = l->sig;
        while(p!=NULL){
            if(p->dato<min)
                min = p->dato;
            p = p->sig;
        }
        printf("\nEl minimo es %d", min);
    }
}

//Opcion 4 del menu
void mayores(nodo *l){
    int n, cant = 0; 
    nodo *p;
	
    printf("\nIngrese el nro: ");
    scanf("%d", &n);
    p = l; 
    while(p!=NULL){
        if(p->dato>n){
            printf("\n%d", p->dato);
            cant++;          
        }
        p = p->sig;
    }
    printf("\nHubo %d nros mayores", cant);
}

//Imprime el menu y devuelve la opcion ingresada
int menu(){
    int op;
    printf("\n1-Agregar\n2-Eliminar\n3-Minimo");
    printf("\n4-Mayores\n5-Salir");
    printf("\nIngrese opcion: ");
    scanf("%d", &op);
    return op;
}     

//Programa principal
int main(){
    nodo *lis = NULL;
    FILE *f;
    int n, val, op;

    f = fopen("muestras.dat", "r");
    if(f==NULL){
        printf("\nError al abrir");
        return 1;
    }
    //Comienza un ciclo donde se lee de a 1 entero y se agrega al final de la lista
    n = fread(&val, sizeof(int), 1, f);
    while(n==1){
        lis=insertar_fifo(lis, val);
        n = fread(&val, sizeof(int), 1, f);
    }
    if(fclose(f)!=0){
        printf("\nError al cerrar");
        return 2;
    }
    do{
        op = menu();
        switch(op){
            case 1:
                 lis = agregar(lis);
                 break;
            case 2:
                 lis = borrar(lis);
                 break;
            case 3:
                 minimo(lis); 
                 break;
            case 4:
                 mayores(lis);
                 break;
            case 5:
                 break;
            default:
                 printf("\nOp invalida");
        }//switch
    } while(op!=5);
    lis = destruir(lis);
    return 0; 
}//main
