
// Escribir una función que busque un elemento de la lista, por comparación con una clave e
// indique si se encuentra o no. Si se encuentra se informará este elemento y también el anterior
// (en caso de existir)

#include<stdio.h>
#include<stdlib.h>


//Declaración de la estructura para los nodos (tipo de datos)
typedef struct nodo {
    int dato;
    struct nodo *sig;
} nodo;

//Funcion que inserta al principio
nodo *insertar_lifo (nodo *l, int d)
    {
    nodo *nuevo;

    nuevo = (nodo*) malloc (sizeof (nodo));
    nuevo->dato = d;
    nuevo->sig = l;
    return nuevo;
    }

//Funcion que muestra los elementos de la lista
void mostrar (nodo *l) {
    printf ("Lista");
    while (l != NULL) {
        printf (" --> %d", l->dato);
        l = l->sig;
    }
    printf ("\n");
}

//Funcion que libera toda la memoria ocupada por la lista
nodo *destruir (nodo *l) {
    nodo * aux;

    while (l != NULL) {
        aux = l;
        l = l->sig;
        free (aux);
    }
    printf ("lista destruida\n");
    return NULL;
}


void busqueda(nodo *lista, int clave)
{

  if(!lista){
    printf("%s\n","La lista está vacia");
    return;
  }

  if(lista->dato == clave){
      printf("El dato encontrado es el primero: %d\n", lista->dato);
      return;
  }

  while (lista->sig!=NULL && lista->sig->dato != clave) {
      // printf("%d\n",lista->dato);
      lista = lista->sig;
  }

  if(!lista->sig){
    printf("%s\n","No se encuentra el dato.");
    return;
  }

  // el caso en el que l es el anterior al dato que queres buscar.
  printf("El anterior es: %d\n",lista->dato);
  printf("El dato encontrados es: %d\n",lista->sig->dato);
}


//Programa principal que hace uso de las funciones definidas arriba
int main ()
    {
    nodo *lista = NULL;

    //Insertar en la lista valores de ejemplo
    lista = insertar_lifo (lista, 4); //Inserta el valor 1
    lista = insertar_lifo (lista, 2); //Inserta el valor 1
    lista = insertar_lifo (lista, 4); //Inserta el valor 1
    lista = insertar_lifo (lista, 3); //Inserta el valor 1
    lista = insertar_lifo (lista, 2); //Inserta el valor 1

    //Mostrar la lista
    mostrar (lista);
    busqueda(lista, 4);
    //Destruir lista
    lista = destruir (lista);

    return 0;
    }
