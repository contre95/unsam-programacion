#include <stdio.h>
// #define SIZE_VECT = 10
// #define VECTOR =

// En este caso no se usa el puntero
void como_en_info() {
  int v[10] = {1,2,3,4,5,6,7,8,9,10};
  // int *p;
  size_t n =  sizeof(v)/sizeof(v[0]);
  // printf("Tamaño del vector: %ld\n",n);
  printf("{");s
  for (int i = n-1 ; i>=0 ; i--){
    printf("%d,",v[i]);
  }
  printf("}");
}

void como_en_prog() {
  int v[10] = {1,2,3,4,5,6,7,8,9,10};
  int *p = v+9;

  // size_t n =  sizeof(v)/sizeof(v[0]); //esto es 10
  // printf("Tamaño del vector: %ld\n",n);
  for ( int i = 9 ; i>=0 ; i--){
    printf("Elemnto %d: %d \n",i,*p);
    p--;
  }

}

int main(int argc, char const *argv[]) {
  printf("\n\n%s\n","como en informática:");
  como_en_info();
  printf("\n\n%s\n","como en programación:");
  como_en_prog();
  return 0;
}
