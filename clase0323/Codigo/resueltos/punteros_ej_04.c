/*
Ej. 4 Punteros
Escribir una función que reciba un vector de enteros y su tamaño, y devuelva la cantidad de números impares que contiene.
Programacion UNSAM
Autor: David Lopez
Año: 2016
*/

#include <stdio.h>
int impares (int *v, int tam)
	{
	int i;
	int imp = 0;

	for (i = 0; i < tam; i++)
		{
		if (*(v + i) % 2 != 0)
			 imp++;
		}
	return imp;
	}

int main()
	{
	int v [10] = {1,2,3,4,5,6,7,8,9,10};
	
	printf ("Cant impares: %d\n", impares (v, 10));
	return 0;
	}
