/*
Ejercicio 10 listas
Programa que inserta ordenado en lista doble
Autor: David Lopez
A�o: 2019
*/
#include <stdio.h>
#include <stdlib.h> //para malloc y system


typedef struct nodo_d
{
    float dato; //Se decidio arbitrariamente usar float para el dato
    struct nodo_d *ant, *sig;
} nodo_d;


//La lista doble esta representada por una estructura con 2 punteros: primero y ultimo
typedef struct ldoble
{
    nodo_d *prim, *ult;
} lista;


lista insertar_lifo(lista l, float d)
{
    nodo_d *nuevo;
    nuevo = (nodo_d*) malloc(sizeof(nodo_d));
    nuevo->dato = d;
    nuevo->ant = NULL;
    nuevo->sig = l.prim;

    if (l.prim == NULL) //Lista vacia, l.prim y l.ult son NULL
    	l.ult = nuevo;
    else
    	l.prim->ant = nuevo;

    l.prim = nuevo;
    return l;
}



//Inserta al final del la lista, muy similar a insertar_lifo, ya que

//la estructura de una lista doblemente enlazada es simetrica

//solo cambia ant por sig,y viceversa, prim por ult y viceversa

lista insertar_fifo(lista l, float d)
{
    nodo_d *nuevo;
    nuevo = (nodo_d*) malloc(sizeof(nodo_d));
    nuevo->dato = d;
    nuevo->sig = NULL;
    nuevo->ant = l.ult;

    if (l.prim == NULL) //Lista vacia, l.prim y l.ult son NULL
	l.prim = nuevo;
    else
	l.ult->sig = nuevo;

    l.ult = nuevo;
    return l;
}


//Destruye la lista liberando la memoria ocupada por cada nodo
lista destruir (lista l)
{
    nodo_d *p, *aux;
    p = l.prim;

    while (p != NULL) {
    	aux = p;
    	p = p->sig;
    	free (aux);
    }

    printf ("Lista destruida\n");
    l.prim = NULL;
    l.ult = NULL;
    return l;
}

nodo_d* nodo_crear(float d){
  nodo_d *nodo;
  nodo = (nodo_d*) malloc(sizeof(nodo_d));
  nodo->sig = NULL;
  nodo->ant = NULL;
  nodo->dato = d;
  return nodo;
}

lista insert_index(lista l, int i, float d)
{
    nodo_d *p;
    // Creo el nuevo nodo con el dato
    nodo_d *nuevo = nodo_crear(d);
    if(!nuevo){
      return l;
    }

    if(l.prim == NULL){
      l.prim = nuevo;
      l.prim = nuevo;
      return l;
    }

    // Itero la lista hasta el indice indicado
    p = l.prim;
    int current_i = 0;

    // Esto while me devuelte un puntero en la posicion i o en el ultimo elemento si la posicion es out of range.
    while (p->sig!=NULL && current_i<i-1) {
      current_i++;
      p = p->sig;
    }

    // Caso particular que sea la ultima posicion
    if(p->sig==NULL){
      l.ult = nuevo;
    }
    else{
      p->sig->ant = nuevo;
      nuevo->sig = p->sig;
    }
    nuevo->ant = p;
    p->sig = nuevo;

    if(i==0){
      printf("\n\n%s\n\n","Ingresaste el indice 0");
      l.prim = nuevo;
    }

    return l;
}

int main() {
	nodo_d *p;
	float f;
  int i;
	lista l;

	l.prim = NULL;
	l.ult = NULL;

	printf("Ingrese: dato (0 para terminar)\n");
	scanf("%f", &f);
	while (f > 0) {
		l = insertar_fifo(l, f);
		scanf("%f", &f);
	}

	printf("Muestro en orden ingresado\n");
	for (p = l.prim; p != NULL; p = p->sig)
		printf("Dato = %6.2f\n", p->dato);

  printf("Ingrese: dato: "); scanf("%f", &f);
  printf("Ingrese: el indice: "); scanf("%d", &i);
  l = insert_index(l,i,f);

  printf("Muestro la lita despues de insertar indice\n");
  for (p = l.prim; p != NULL; p = p->sig)
    printf("Dato = %6.2f\n", p->dato);

	l = destruir (l);
	return 0;
}
