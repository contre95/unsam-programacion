/*
Ej 2 practica listas
Escriba una función que devuelva la cantidad de elementos de una lista lineal.
Programacion UNSAM
Autor: David Lopez
Año: 2016
*/

#include<stdio.h>
#include<stdlib.h>

//Declaración de la estructura para los nodos (tipo de datos)
typedef struct nodo {
    int dato;
    struct nodo *sig;   
} nodo;

//Cuenta la cantidad de nodos
//Esta funcion es lo que se pide en el enunciado
//Todo lo demás es para probarla
int cuenta_nodos (nodo *l) {
    int n = 0;

    while (l != NULL) {
        n++;
        l = l->sig;
    }
    return n;
}
    
//Inserta al principio
nodo *insertar_lifo (nodo *l, int d)
    {
    nodo *nuevo;

    nuevo = (nodo*) malloc (sizeof (nodo));
    nuevo->dato = d;
    nuevo->sig = l;
    return nuevo;
    }   

//Muestra los elementos de la lista
void mostrar (nodo *l) {
    printf ("Lista");
    while (l != NULL) {
        printf (" --> %d", l->dato);
        l = l->sig;
    }
    printf ("\n");
}


//Libera toda la memoria ocupada por la lista
nodo *destruir (nodo *l) {
    nodo * aux;

    while (l != NULL) {
        aux = l;
        l = l->sig;
        free (aux);
    }
    printf ("lista destruida\n");
    return NULL;
}

//Pide entros hasta que se ingrese cero y los inserta en la lista
nodo *ingresa_usuario(nodo *lista) {
    int temp;

    printf ("Ingrese un nro entero (cero finaliza):");
    scanf ("%d", &temp);
    while (temp != 0) {
        lista = insertar_lifo (lista, temp);
        printf ("\nIngrese otro nro entero (cero finaliza):");
        scanf ("%d", &temp);
    }
    return lista;   
}

//Programa principal que hace uso de las funciones definidas arriba
int main ()
    {
    nodo *lista = NULL;

    //Inserta en la lista los valores que ingrese el usuario
    lista = ingresa_usuario (lista);
    mostrar (lista);

    //Invoca la funcion cuenta_nodos
    printf ("La cantidad de datos es %d\n", cuenta_nodos (lista));

    //Destruir lista
    lista = destruir(lista);

    return 0;
    }
