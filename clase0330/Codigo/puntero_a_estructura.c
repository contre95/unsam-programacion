/*
Programacion UNSAM
Autor: David Lopez
Año: 2019
Ejemplo de punteros con estructuras:
Este programa permite ingresar por teclado los datos (codigo, nombre, precio y stock)
de un producto. 
Luego se muestra por pantalla de dos formas distintas usando un puntero.
*/

#include <stdio.h>

//Aca se define el tipo de datos
struct producto {	
	int codigo;
	char nombre [10];
	float precio;
	long stock;
};

//Esta funcion recibe un puntero a estructura de tipo producto y muestra sus campos
void muestra_producto (struct producto *p) {
	printf ("Codigo: %d - Nombre: %s - Precio %.2f - Stock: %ld\n", (*p).codigo, (*p).nombre, (*p).precio, (*p).stock);
}

//Esta es identica a la anterior per escrita de otra forma: "p->" en lugar de "(*p)."
void muestra_producto2 (struct producto *p) {
	printf ("Codigo: %d - Nombre: %s - Precio %.2f - Stock: %ld\n", p->codigo, p->nombre, p->precio, p->stock);
}

int main () {
	struct producto prod;
   struct producto *p;

	//Ingresar cod
	printf ("Ingrese un codigo: ");
	scanf ("%d", &prod.codigo);
   //ingresar otros campos
   printf ("Ingrese el nombre del producto: ");
   scanf ("%s", prod.nombre);
   printf ("Ingrese el precio: ");
   scanf ("%f", &prod.precio);
   printf ("Ingrese el stock: ");
   scanf ("%ld", &prod.stock);
  //Recien aca empiezo a usar el puntero
  p = &prod;
	muestra_producto (p);
	muestra_producto2 (p);

	return 0;
}

