/*
Ej 13 Escribir un programa para leer el archivo vector.dat del ejercicio anterior y almacenarlo en un vector. 
No se conoce la cantidad de elementos y no se puede limitar.
Programacion UNSAM
Autor: David Lopez
Año: 2016
*/

#include <stdio.h>
#include <stdlib.h>

/*************************************************/
int main () {
	FILE *f;
	int r;
	int *v; 
	int i, j=0;

	v = (int*) malloc (sizeof (int)); //Reservo memoria para un vector de 1 elmento
	//Verifico que se haya podido reservar memoria
	if (v == NULL) {
		printf ("Error reservando memoria");
		return -1;
	}

	//Abro el archivo para lectura
	f = fopen ("vector.dat", "r");
	if (f == NULL) {
		printf ("Error abriendo");
		return -2;
	}

	r = fread (v, sizeof (int), 1, f);
	while (r == 1) { //Continuo el ciclo hasta que fread no lea nada
		j++;
		//Por simplicidad omito verificar lo que devuelve realloc
		//Les queda como tarea
		v = (int*) realloc (v, (j + 1) * sizeof (int));
		r = fread (v + j, sizeof (int), 1, f);
	}   

	//Cierro el archivo
	if (fclose (f) != 0)
		printf ("Error cerrando");

	//Muestro los elementos del vector
	for (i = 0; i < j; i++)
		printf ("%d\n", v [i]);

	free (v); //Libero la memoria liberada
	return 0;
}
