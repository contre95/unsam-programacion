/*
Ejercicio 12 Punteros y Memoria dinamica:
Escribir un prog. en el que el usuario informa la cantidad de elementos que va a ingresar 
y luego los valores de los elementos (enteros).
Los elementos deben almacenarse en un vector y luego en el archivo vector.dat. 
No se debe limitar la cantidad de elementos.
Programacion UNSAM
Autor: David Lopez
Año: 2017
*/

#include <stdio.h>
#include <stdlib.h>

/*
Esta funcion recibe un vector de enteros y la cantidad, y lo graba en el archivo
*/
void grabar (int *v, int n) {
	FILE *f;
	int w;
	
	f = fopen ("vector.dat", "w");
	if (f != NULL) {
		w = fwrite (v, sizeof (int), n, f);
		if (w != n)
			printf ("Error grabando");
		if (fclose (f) != 0)
			printf ("Error cerrando");
	}
	else 
		printf ("Error creando");
}

int main () {
	int n, i;
	int *p;

	printf ("Ingrese la cantidad de elementos:");
	scanf ("%d", &n);
	p = (int*) malloc(sizeof(int) * n);
	if (p != NULL) {
		for (i = 0; i < n; i++) {
			printf ("\nIngrese el elemento:");
			scanf ("%d", p + i); //Puntero al i-esimo elemento
		}
	}
	
	grabar (p, n);
	free (p);
	return 0;
}
