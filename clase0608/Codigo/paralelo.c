/* Este programa toma un dato de un caracter, recibido como argumento por linea de comando,
lo niega y lo envia al buffer del puerto de impresora, luego pone el 
strobe a cero para que el dato pase a los leds, y finalmente normaliza 
el strobe a uno.  */ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/io.h>

/* Esta funcion otorga/quita (segun <permiso> sea 1/0) privilegios a <cantidad> de puertos a partir del puerto <base> */
void perm(unsigned long base, unsigned long cantidad, int permiso){
    if (ioperm(base, cantidad, permiso) != 0) {
        perror("ioperm");
        exit(1);
    }
}

int main(int argc, char **argv) {
    char j;

    perm(0x378, 3, 1);   /* port, num, true */
    j = argv[1][0];
    printf("Dato mostrado en los leds %hhx\n",j);
    j = ~j;
    outb(j,0x378); //Saco por el registro de datos
    printf("Salida del dato por puerto : %hhx \n",j);
    /* strobe 0 */
    outb(0x00,0x37A); //Registro de control. Lo correcto seria tocar solo el bit strobe. ARREGLAR PARA EL TP!!!
    usleep(1);
    /* strobe 1 */
    outb(0x01,0x37A);
    perm(0x378, 3, 0);   /* port, num, false */

    return 0;
} 
